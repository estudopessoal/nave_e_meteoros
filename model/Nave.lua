require('../util/config')

Nave = {
    src = 'images/14bis.png',
    width = 56,
    height = 62,
    weight = 3,
    alive = true,
    position = {},
    audio = {
        destroy = {
            src = 'audios/destruicao.wav',
            type = 'static'
        }
    },
    move = function(self)

        if not self.alive then return end    

        if love.keyboard.isDown('w', 'up') and self.position.y > 0 then
            self.position.y = self.position.y - self.weight
        end

        if love.keyboard.isDown('a', 'left') and self.position.x > 0 then
            self.position.x = self.position.x - self.weight
        end
        
        if love.keyboard.isDown('s', 'down') and self.position.y < WINDOW_HEIGHT - self.height then
            self.position.y = self.position.y + self.weight
        end
        
        if love.keyboard.isDown('d', 'right')  and self.position.x < WINDOW_WIDTH - self.width then
            self.position.x = self.position.x + self.weight
        end
    end,
    destroy = function(self)
    
        self.alive  = false
        self.src    = 'images/explosao_nave.png'
        self.image  = love.graphics.newImage(self.src)
        self.width  = 67
        self.height = 77

        self.audio.destroy.music:play()

    end
}