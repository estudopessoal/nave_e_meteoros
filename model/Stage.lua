require('../util/config')

Stage = {
    src = 'images/background.png',
    position = {
        x = 0,
        y = 0
    },
    audio = {
        default = {
            src = 'audios/ambiente.wav',
            type = 'static'
        },
        gameover = {
            src = 'audios/game_over.wav',
            type = 'static'
        },
        victory = {
            src = 'audios/winner.wav',
            type = 'static'
        }
    },
    fail = function(self)
        self.audio.default.music:stop()
        self.audio.gameover.music:play()
    end,
    checkMission = function(self)

        if METEOROS_DESTRUIDOS >= METEOROS_TOTAL then
            self.audio.default.music:stop()
            self.audio.victory.music:play()
            VITORIA = true
        end

    end
}