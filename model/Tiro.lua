Tiro = {
  src = 'images/tiro.png',
  width = 12,
  height = 12,
  weight = 5,
  position = {},
  audio = {
    default = {
      src = 'audios/disparo.wav',
      type = 'static'
    }
  }
}