require('../util/config')

Loose = {
  src = 'images/gameover.png',
  position = {
    x = (WINDOW_WIDTH - 204)/2,
    y = (WINDOW_HEIGHT - 148)/2 
  },
  width = 204,
  height = 148
}