require('./util/config')
require('./model/Stage')
require('./model/Loose')
require('./model/Winner')
require('./model/Meteoro')
require('./model/Nave')
require('./model/Tiro')
require('./factory/TiroFactory')
require('./factory/MeteoroFactory')

function love.load()

    math.randomseed(os.time())

    love.window.setTitle(GAME_TITLE)
    love.window.setMode(WINDOW_WIDTH, WINDOW_HEIGHT, { resizable = false })

    Stage.image   = love.graphics.newImage(Stage.src)
    Meteoro.image = love.graphics.newImage(Meteoro.src)
    Nave.image    = love.graphics.newImage(Nave.src)    
    Tiro.image    = love.graphics.newImage(Tiro.src)    
    Loose.image   = love.graphics.newImage(Loose.src)
    Winner.image   = love.graphics.newImage(Winner.src)

    Nave.position.x = (WINDOW_WIDTH - Nave.height)/2
    Nave.position.y = WINDOW_HEIGHT - Nave.width

    Stage.audio.default.music  = love.audio.newSource(Stage.audio.default.src, Stage.audio.default.type)
    Stage.audio.victory.music  = love.audio.newSource(Stage.audio.victory.src, Stage.audio.victory.type)
    Stage.audio.gameover.music = love.audio.newSource(Stage.audio.gameover.src, Stage.audio.gameover.type)
    Nave.audio.destroy.music   = love.audio.newSource(Nave.audio.destroy.src, Nave.audio.destroy.type)
    Tiro.audio.default.music   = love.audio.newSource(Tiro.audio.default.src, Tiro.audio.default.type)

    fonteObjetivo = love.graphics.newFont(12)
    fonteContador = love.graphics.newFont(30)

    Stage.audio.default.music:setLooping(true)
    Stage.audio.default.music:play()
end

function love.update(dt)

    if GAME_OVER or VITORIA then
        return
    end

    if love.keyboard.isDown('w','a','s','d', 'up', 'left', 'down', 'right') then
        Nave:move()
    end

    TiroFactory:move()
    TiroFactory:checkCollision()
    TiroFactory:remove()

    MeteoroFactory:create()
    MeteoroFactory:move()
    MeteoroFactory:checkCollision()
    MeteoroFactory:remove()

end
 
function love.keypressed(key)

    if key == 'escape' then
        love.event.quit()
    end

    if key == 'space' and not GAME_OVER then
        TiroFactory:create()
    end
end

function love.draw()

    love.graphics.draw(Stage.image, Stage.position.x, Stage.position.y)
    love.graphics.draw(Nave.image, Nave.position.x, Nave.position.y)
    
    for k, item in pairs(MeteoroFactory.list) do
        love.graphics.draw(Meteoro.image, item.position.x, item.position.y)
    end
    
    for k, item in pairs(TiroFactory.list) do
        love.graphics.draw(Tiro.image, item.position.x, item.position.y)
    end

    if GAME_OVER then
        love.graphics.draw(Loose.image, Loose.position.x, Loose.position.y)
    end

    if VITORIA then
        love.graphics.draw(Winner.image, Winner.position.x, Winner.position.y)
    end

    love.graphics.setFont(fonteObjetivo)
    love.graphics.print('Objetivo: Destrua ' .. METEOROS_TOTAL .. ' meteoros', 10, 10)
    love.graphics.setFont(fonteContador)
    love.graphics.print(METEOROS_DESTRUIDOS, WINDOW_WIDTH - 30, WINDOW_HEIGHT - 40)

end