require('../util/config')
require('../model/Nave')
require('../model/Tiro')
require('../model/Stage')
require('../factory/MeteoroFactory')

TiroFactory = {
  list = {},
  limit = 3,
  create = function(self)

    if #self.list >= self.limit then return end

    novoTiro = {
      src = Tiro.src,
      width = Tiro.width,
      height = Tiro.height,
      weight = Tiro.weight,
      position = {
        x = Nave.position.x + (Nave.width - Tiro.width)/2,
        y = Nave.position.y - Tiro.height
      }
    }
    
    table.insert( self.list, novoTiro)

    if love.audio.getActiveSourceCount() > 1 then
      Tiro.audio.default.music:stop()
    end

    Tiro.audio.default.music:play()

  end,
  move = function(self)

    for index, tiro in pairs(self.list) do
      tiro.position.y = tiro.position.y - tiro.weight
    end

  end,
  remove = function(self)

    for index, tiro in pairs(self.list) do
      if tiro.position.y < 0 - tiro.height then
        table.remove( self.list, index)
        return
      end
    end

  end,
  checkCollision = function(self)

    for k_tiro, tiro in pairs(self.list) do
      for k_meteoro, meteoro in pairs(MeteoroFactory.list) do
        if hasCollision(
          tiro.position.x,    tiro.position.y,    tiro.width,    tiro.height, 
          meteoro.position.x, meteoro.position.y, meteoro.width, meteoro.height
        ) then
          
          table.remove( self.list, k_tiro)
          table.remove( MeteoroFactory.list, k_meteoro)
          METEOROS_DESTRUIDOS = METEOROS_DESTRUIDOS + 1
          Stage:checkMission()

        end
      end
    end

  end
}