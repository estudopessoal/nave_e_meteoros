require('../util/config')
require('../util/hasCollision')
require('../model/Meteoro')
require('../model/Nave')
require('../model/Stage')

MeteoroFactory = {
    list = {},
    limit = 10,
    create = function(self)

        if #self.list >= self.limit then return end

        novoMeteoro = {
            width = Meteoro.width,
            height = Meteoro.height,
            weight = math.random(2),
            horizontal_movement = math.random(-1, 1),
            position = {
                x = math.random(WINDOW_WIDTH - 64),
                y = -Meteoro.height
            }
        }
        
        table.insert( self.list, novoMeteoro)
        
    end,
    move = function(self)

        for index, meteoro in pairs(self.list) do
            
            meteoro.position.y = meteoro.position.y + meteoro.weight
            meteoro.position.x = meteoro.position.x + meteoro.horizontal_movement

        end

    end,
    remove = function(self)

        for index, meteoro in pairs(self.list) do
            
            if meteoro.position.y >= WINDOW_HEIGHT then
                table.remove( self.list, index)
                return
            end

        end

    end,
    checkCollision = function(self)

        for k, meteoro in pairs(self.list) do
            if hasCollision(
                meteoro.position.x, meteoro.position.y, meteoro.width, meteoro.height, 
                Nave.position.x,    Nave.position.y,    Nave.width,    Nave.height
            ) then
                
                GAME_OVER = true
                Stage:fail()
                Nave:destroy()

            end
        end

    end
}